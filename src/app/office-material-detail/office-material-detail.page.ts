import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { MaterialsService } from '../materials.service';
import { Material } from '../models/material.model';

@Component({
  selector: 'app-office-material-detail',
  templateUrl: './office-material-detail.page.html',
  styleUrls: ['./office-material-detail.page.scss']
})
export class OfficeMaterialDetailPage implements OnInit {

  loadedMaterial: Material;

  constructor(private route: ActivatedRoute, private navCtrl: NavController, private materialSrvc: MaterialsService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('materialId')) {
        this.navCtrl.navigateBack('materials');
        return;
      }

      // this.materialSrvc.getMaterial(paramMap.get('materialId'));

      this.materialSrvc.getMaterial(paramMap.get('materialId')).subscribe(
        material => {
          this.loadedMaterial = material;
        },
        error => {
          console.log(error);
        }
      );
    });
  }
}
