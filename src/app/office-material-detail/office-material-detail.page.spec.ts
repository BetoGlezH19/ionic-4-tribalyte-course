import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeMaterialDetailPage } from './office-material-detail.page';

describe('OfficeMaterialDetailPage', () => {
  let component: OfficeMaterialDetailPage;
  let fixture: ComponentFixture<OfficeMaterialDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeMaterialDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeMaterialDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
