import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OfficeMaterialDetailPage } from './office-material-detail.page';

const routes: Routes = [
  {
    path: '',
    component: OfficeMaterialDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OfficeMaterialDetailPage]
})
export class OfficeMaterialDetailPageModule {}
