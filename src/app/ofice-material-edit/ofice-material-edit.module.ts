import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OficeMaterialEditPage } from './ofice-material-edit.page';

const routes: Routes = [
  {
    path: '',
    component: OficeMaterialEditPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OficeMaterialEditPage]
})
export class OficeMaterialEditPageModule {}
