import { Component, OnInit } from '@angular/core';
import { MaterialsService } from '../materials.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { Material } from '../models/material.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ofice-material-edit',
  templateUrl: './ofice-material-edit.page.html',
  styleUrls: ['./ofice-material-edit.page.scss']
})
export class OficeMaterialEditPage implements OnInit {
  material: Material;
  form: FormGroup;

  constructor(
    private materialsSrvc: MaterialsService,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private toastCtrl: ToastController
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.get('materialId')) {
        this.navCtrl.navigateBack('materials');
        return;
      }

      this.materialsSrvc
        .getMaterial(paramMap.get('materialId'))
        .subscribe(material => {
          this.material = material;

          this.form = new FormGroup({
            title: new FormControl(this.material.name, {
              updateOn: 'blur',
              validators: [Validators.required]
            }),
            description: new FormControl(this.material.description, {
              updateOn: 'blur',
              validators: [Validators.required, Validators.maxLength(100)]
            }),
            elementNumber: new FormControl(this.material.quantity, {
              updateOn: 'blur',
              validators: [Validators.required, Validators.min(1)]
            })
          });
        });
    });
  }

  onEditOffer() {
    if (!this.form.valid) {
      return;
    }

    this.materialsSrvc
      .updateMaterial(
        this.material.id,
        this.form.value.title,
        this.form.value.description,
        this.form.value.elementNumber
      )
      .subscribe(res => {
        console.log(res);

        this.toastCtrl.create({
          animated: true,
          position: 'top',
          showCloseButton: true,
          message: 'El material se editó correctamente',
          duration: 4000,
        }).then(toastEl => {
          toastEl.present();
          this.router.navigate(['/materials']);
        });
      });
  }
}
