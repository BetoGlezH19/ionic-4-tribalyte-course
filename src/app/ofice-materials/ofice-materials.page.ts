import { Component, OnInit } from '@angular/core';
import { MaterialsService } from '../materials.service';
import { Material } from '../models/material.model';
import { IonItemSliding } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ofice-materials',
  templateUrl: './ofice-materials.page.html',
  styleUrls: ['./ofice-materials.page.scss'],
})
export class OficeMaterialsPage implements OnInit {

  loadedMaterials: Material[];

  constructor(private materialSrvc: MaterialsService, private router: Router) { }

  ngOnInit() {
    this.materialSrvc.materials.subscribe(materials => {
      this.loadedMaterials = materials;
    });
  }

  onEditMaterial(materialId: string, slidingItem: IonItemSliding) {
    this.router.navigate(["/", "materials", "edit", materialId]);
    slidingItem.close();
  }

}
