import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OficeMaterialsPage } from './ofice-materials.page';

const routes: Routes = [
  {
    path: '',
    component: OficeMaterialsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OficeMaterialsPage]
})
export class OficeMaterialsPageModule {}
