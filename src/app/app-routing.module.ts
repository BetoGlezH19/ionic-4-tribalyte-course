import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'materials',
    children: [
      {
        path: '',
        loadChildren: './ofice-materials/ofice-materials.module#OficeMaterialsPageModule'
      },
      {
        path: 'info/:materialId',
        loadChildren: './office-material-detail/office-material-detail.module#OfficeMaterialDetailPageModule'
      },
      {
        path: 'edit/:materialId',
        loadChildren: './ofice-material-edit/ofice-material-edit.module#OficeMaterialEditPageModule'
      },
      {
        path: '',
        redirectTo: '/materials',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/materials',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
